package br.com.exercicioZipkin.consultaCep.controller;

import br.com.exercicioZipkin.consultaCep.models.Cep;
import br.com.exercicioZipkin.consultaCep.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CepController {

    @Autowired
    private CepService cepService;

    @GetMapping("/consultaCep/{cep}")
    public Cep consultaCep(@PathVariable String cep) {

        return cepService.consultarCep(cep);
    }

}
