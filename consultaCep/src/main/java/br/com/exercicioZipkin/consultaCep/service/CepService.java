package br.com.exercicioZipkin.consultaCep.service;

import br.com.exercicioZipkin.consultaCep.microservicoExterno.CepViacep;
import br.com.exercicioZipkin.consultaCep.models.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepViacep cepViacep;

    @NewSpan(name = "viacep-service")
    public Cep consultarCep(@SpanTag("cep") String cep) {
        return cepViacep.consultarCep(cep);
    }

}
