package br.com.exercicioZipkin.consultaCep.microservicoExterno;

import br.com.exercicioZipkin.consultaCep.models.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/")
public interface CepViacep {

    @GetMapping("/ws/{cep}/json/")
    Cep consultarCep(@PathVariable String cep);
}
