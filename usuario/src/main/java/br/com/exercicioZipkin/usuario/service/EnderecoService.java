package br.com.exercicioZipkin.usuario.service;

import br.com.exercicioZipkin.usuario.models.Endereco;
import br.com.exercicioZipkin.usuario.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService {

    @Autowired
    private EnderecoRepository enderecoRepository;

    @NewSpan(name = "cadastrar-endereco")
    public Endereco gravarEnderecoUsuario(Endereco endereco){
        return enderecoRepository.save(endereco);
    }
}
