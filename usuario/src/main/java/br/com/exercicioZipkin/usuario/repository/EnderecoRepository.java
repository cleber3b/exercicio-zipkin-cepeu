package br.com.exercicioZipkin.usuario.repository;

import br.com.exercicioZipkin.usuario.models.Endereco;
import org.springframework.data.repository.CrudRepository;

public interface EnderecoRepository extends CrudRepository<Endereco, Integer> {
}
