package br.com.exercicioZipkin.usuario.service;

import br.com.exercicioZipkin.usuario.microservicoExterno.Cep;
import br.com.exercicioZipkin.usuario.microservicoExterno.UsuarioConsultaCep;
import br.com.exercicioZipkin.usuario.models.Endereco;
import br.com.exercicioZipkin.usuario.models.Usuario;
import br.com.exercicioZipkin.usuario.models.dto.CriarUsuarioResponse;
import br.com.exercicioZipkin.usuario.models.dto.UsuarioMapper;
import br.com.exercicioZipkin.usuario.repository.EnderecoRepository;
import br.com.exercicioZipkin.usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioConsultaCep usuarioConsultaCep;

    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private UsuarioMapper usuarioMapper;

    @Transactional
    @NewSpan(name = "cadastar-usuario")
    public CriarUsuarioResponse cadastrarUsuario(Usuario usuario) {

        return usuarioMapper.paraCriarUsuarioResponse(usuarioRepository.save(usuario), this.cadastrarEndereco(usuario.getCep()));

    }

    @NewSpan(name = "cadastrar-endereco-usuario")
    private Endereco cadastrarEndereco(String cepUsuario) {

        Cep cep = this.consultaCepExterno(cepUsuario);

        Endereco endereco = new Endereco();
        endereco.setRua(cep.getLogradouro());
        endereco.setBairro(cep.getBairro());
        endereco.setCep(cep.getCep());
        endereco.setCidade(cep.getUf());
        endereco.setComplemento(cep.getComplemento());
        endereco.setEstado(cep.getUf());

        return enderecoService.gravarEnderecoUsuario(endereco);
    }

    @NewSpan(name = "consulta-cep-externo")
    private Cep consultaCepExterno(String cepUsuario) {
        return usuarioConsultaCep.consultarCep(cepUsuario);
    }


}
