package br.com.exercicioZipkin.usuario.models.dto;

import br.com.exercicioZipkin.usuario.models.Endereco;

import java.util.List;

public class CriarUsuarioResponse {

    private Integer id;

    private String nome;

    private String email;

    private List<Endereco> listaEnderecos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Endereco> getListaEnderecos() {
        return listaEnderecos;
    }

    public void setListaEnderecos(List<Endereco> listaEnderecos) {
        this.listaEnderecos = listaEnderecos;
    }
}
