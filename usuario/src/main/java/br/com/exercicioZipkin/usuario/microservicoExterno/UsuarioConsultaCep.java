package br.com.exercicioZipkin.usuario.microservicoExterno;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "consultaCep", url = "http://localhost:7070/rotazuulconsultaCep/consultaCep/")
public interface UsuarioConsultaCep {

    @GetMapping("/{cep}")
    Cep consultarCep(@PathVariable String cep);
}
