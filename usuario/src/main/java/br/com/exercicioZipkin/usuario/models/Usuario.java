package br.com.exercicioZipkin.usuario.models;

import br.com.exercicioZipkin.usuario.microservicoExterno.Cep;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message ="Nome precisa ser preenchido.")
    @Size(min = 3, message = "Nome precisa ter no mínimo 3 letras.")
    @NotBlank(message = "Nome precisa ser preenchido.")
    private String nome;

    @Email
    private String email;

    @NotNull(message ="CEP precisa ser preenchido.")
    @NotBlank(message = "CEP precisa ser preenchido.")
    private String cep;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
