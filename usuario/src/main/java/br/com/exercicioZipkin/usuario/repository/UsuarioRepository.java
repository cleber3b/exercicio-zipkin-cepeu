package br.com.exercicioZipkin.usuario.repository;

import br.com.exercicioZipkin.usuario.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
