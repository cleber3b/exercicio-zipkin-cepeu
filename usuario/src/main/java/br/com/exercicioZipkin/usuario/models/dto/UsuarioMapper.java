package br.com.exercicioZipkin.usuario.models.dto;

import br.com.exercicioZipkin.usuario.models.Endereco;
import br.com.exercicioZipkin.usuario.models.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UsuarioMapper {

    public CriarUsuarioResponse paraCriarUsuarioResponse(Usuario usuario, Endereco endereco){
        CriarUsuarioResponse criarUsuarioResponse = new CriarUsuarioResponse();
        List<Endereco> enderecos = new ArrayList<>();
        enderecos.add(endereco);

        criarUsuarioResponse.setId(usuario.getId());
        criarUsuarioResponse.setNome(usuario.getNome());
        criarUsuarioResponse.setEmail(usuario.getEmail());
        criarUsuarioResponse.setListaEnderecos(enderecos);

        return criarUsuarioResponse;
    }

}
